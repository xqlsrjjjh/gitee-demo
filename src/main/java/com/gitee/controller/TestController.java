package com.gitee.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TestController {

    Logger logger = LoggerFactory.getLogger(TestController.class);

    @RequestMapping("/")
    public Object test(HttpServletRequest request) {

        logger.info("HTTP request : "+request.getRemoteHost());
        String s = "Welcome " + request.getRemoteHost();

        return s;
    }

    @RequestMapping("/info")
    public Object info() {
        logger.info("[HTTP] INFO");
        return "get info success";
    }

}
