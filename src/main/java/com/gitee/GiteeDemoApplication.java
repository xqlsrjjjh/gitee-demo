package com.gitee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiteeDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GiteeDemoApplication.class, args);
    }
}
